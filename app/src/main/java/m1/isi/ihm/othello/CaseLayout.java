



package m1.isi.ihm.othello;

import android.content.Context;
import android.util.Log;
import android.widget.ImageButton;

import m1.isi.ihm.othello.jeu.Case;
import m1.isi.ihm.othello.jeu.SujetCase;


public class CaseLayout extends ImageButton implements CaseObserver {
    private int posX;
    private int posY;
    private int currentJoueur;
    private int pionP1 = R.drawable.piece_white;
    private int pionP2 = R.drawable.piece_black;

    public void defPion(String pion, int player){
        int tmp_pion;
        switch(pion){
            case "Cartman":
                tmp_pion =  R.drawable.sp_cartman;
                break;
            case "Kenny":
                tmp_pion =  R.drawable.sp_kenny;
                break;
            case "Kyle":
                tmp_pion = R.drawable.sp_kyle;
                break;
            case "Stan":
                tmp_pion = R.drawable.sp_stan;
                break;
            default:
                if(pionP1 != pionP2)
                    tmp_pion = R.drawable.piece_white;
                else{
                    tmp_pion = R.drawable.piece_black;
                }
                break;
        }
        if(player == 1){
            this.pionP1 = tmp_pion;
        }else{
            this.pionP2 = tmp_pion;
        }
    }
    public int getPionP1(){
        return this.pionP1;
    }
    public int getPionP2(){
        return this.pionP2;
    }
    public int getCurrentJoueur() {
        return currentJoueur;
    }

    public void setCurrentJoueur(int currentJoueur) {
        this.currentJoueur = currentJoueur;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public CaseLayout(Context context) {
        super(context);
    }

    @Override
    public void actualise(SujetCase caseJeu) {
        Case caseJeu_casted=(Case)caseJeu;
        Log.d("debug","actualise image");
        switch (caseJeu_casted.getJoueur()){
            case 0 :
                if(caseJeu_casted.isJouableJ1()&&currentJoueur==1){
                    this.setImageResource(R.drawable.piece_empty_can_play);
                }
                else if(caseJeu_casted.isJouableJ2()&&currentJoueur==2){
                    this.setImageResource(R.drawable.piece_empty_can_play);
                }
                else{
                    this.setImageResource(R.drawable.piece_empty);
                }
                break;
            case 1: this.setImageResource(pionP1);
                break;
            case 2: this.setImageResource(pionP2);
        }
    }
}
