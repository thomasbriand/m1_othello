package m1.isi.ihm.othello.jeu;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tbriand on 01/03/16.
 */
public class IAmoyenne implements IA {

    private int profondeur;
    private MainJeu simulation;
    public IAmoyenne(int profondeur,MainJeu etatJeu){
        this.profondeur=profondeur;
        this.simulation=new MainJeu(etatJeu);
    }


    @Override
    public Case coupAJoue(List<Case> listCoupValide,MainJeu etatJeu) {
        Case caseRes=listCoupValide.get(0);
        int max=0;
        int temp;
        this.simulation=new MainJeu(etatJeu);
        for(Case c : listCoupValide){
            Log.d("Debug","IA Test Case :"+c.getPosX() + " :" + c.getPosY());
            temp=minMax(c,profondeur,2);
            if (temp >max) {
                caseRes=c;
                max=temp;
            }
        }
        return caseRes;
    }


    private int minMax(Case coupJoue,int prof,int joueur){
        /*minMax vaut 1 si on fait le max et -1 si on fait le min*/
        int minMax;
        ArrayList<Case>nextListCoup;
        int res=0;
        int temp;
        if(joueur==2){
            minMax=1;
        }
        else{
            minMax=-1;
        }
        simulation.coupJoue(joueur,coupJoue.getPosX(),coupJoue.getPosY());
        nextListCoup=simulation.getListCoupJouable(simulation.nextJoueur(joueur));
        if(prof>0 && nextListCoup!=null && !nextListCoup.isEmpty()){
            for(Case c :nextListCoup){
                temp=minMax*minMax(c,prof-1,simulation.nextJoueur(joueur));
                if((minMax==-1 && temp<res)||(minMax==1 && temp>res)){
                    res=temp;
                }
            }
            return res;
        }
        else{
            return minMax*eval();
        }

    }
    private int eval(){
        return simulation.comptePions(1)-simulation.comptePions(2);
    }

}
