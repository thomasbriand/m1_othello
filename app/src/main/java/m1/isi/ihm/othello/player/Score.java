package m1.isi.ihm.othello.player;

import java.util.Date;

/**
 * Created by Vincent on 11/02/2016.
 */
public class Score {
    //Attributs
    private Profile p;
    private Date d;
    private int score;

    //Constructeur
    public Score(Profile p, int score){
        this.p = p;
        this.score = score;
    }
    public Score(Profile p, Date d, int score){
        new Score(p, score);//Je suis pas sur de moi là
        this.score = score;
    }
    //Accesseurs
    public Profile getProfile(){
        return this.p;
    }
    public Date getDate(){
        return this.d;
    }
    public int getScore(){
        return this.score;
    }
}
