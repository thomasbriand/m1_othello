package m1.isi.ihm.othello.jeu;

import m1.isi.ihm.othello.CaseObserver;

/**
 * Created by tbriand on 24/02/16.
 */
public interface SujetCase {
    void registerObserver(CaseObserver obs);
    void removeObserver(CaseObserver obs);
    void notifyObs();
}
