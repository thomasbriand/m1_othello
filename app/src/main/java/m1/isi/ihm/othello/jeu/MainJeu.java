package m1.isi.ihm.othello.jeu;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tbriand on 02/02/16.
 */
public class MainJeu implements Cloneable {
    private enum directions{sud,est,ouest,nord,sud_est,sud_ouest,nord_est,nord_ouest}

    private Grille grille;

    public MainJeu(String data){
        ArrayList<String>dataInList= new ArrayList<String>(Arrays.asList(data.split("\n")));
        grille=new Grille(dataInList.subList(1,dataInList.size()));
        setCoupsJouables();
    }
    public int getDimension(){
        return grille.getDimensionX();
    }
    public MainJeu(Grille grille){this.grille=grille;}
    public MainJeu(int dimX,int dimY){
        grille=new Grille(dimX,dimY);
    }
    public MainJeu(MainJeu toClone){
        grille=new Grille(toClone.getGrille());
    }
    /*Retourne la liste des cases a retourner si le coup fait une prise dnas la liste donnée, retourne null sinon*/
    public ArrayList<Case> faitPrise(int joueur,ArrayList<Case> listCase){
        int i=0;
        boolean adversaireDansList=false;
        ArrayList<Case> caseARetourner=new ArrayList<Case>();
        while(i<listCase.size()&& !listCase.get(i).estVide() && listCase.get(i).getJoueur()!=joueur ){
            if(!adversaireDansList){
                adversaireDansList=true;
            }
            caseARetourner.add(listCase.get(i));
            i++;
        }
        if(i!=listCase.size() && listCase.get(i).getJoueur()==joueur&& !listCase.get(i).estVide()){
            return caseARetourner;
        }
        else{
            return null;
        }
    }
    /*retourne la liste coup a retourner si le coup est valide, null sinon*/
    public ArrayList<Case> coupValide(int joueur,int posX,int posY){
        ArrayList<Case> caseDansDir;
        ArrayList<Case> caseARetourner=new ArrayList<Case>();
        ArrayList<Case> caseARetournerTemp;
        if(grille.getCase(posX,posY).estVide()){
            for(directions dir: directions.values()){
                caseDansDir=grille.getListDansDirection(posX,posY,dir.toString());
                if((caseARetournerTemp=faitPrise(joueur,caseDansDir))!=null){
                    caseARetourner.addAll(caseARetournerTemp);
                }
            }
            if(!caseARetourner.isEmpty()){
                return caseARetourner;
            }
            else{
                Log.d("Debug","Coup non valide");
                return null;
            }
        }
        else{
            Log.d("Debug","Coup non valide");
            return null;
        }

    }
    /*Joue le coup demandée dans la grille, retourne true si le coup est valide et à été joué false sinon*/
    public boolean coupJoue(int joueur,int posX,int posY){
        ArrayList<Case> caseARetourner;
        if((caseARetourner=coupValide(joueur,posX,posY))!=null){
            for(int i=0 ; i <caseARetourner.size();i++){
                caseARetourner.get(i).retourne();
            }
            grille.getCase(posX,posY).joue(joueur);
            setCoupsJouables();
            return true;
        }
        else{
            return false;
        }
    }
    public int comptePions(int joueur){
        return grille.comptePions(joueur);
    }
    public void setCoupsJouables(){
        ArrayList<Case>casesVide;
        Case caseTemp;
        int posX,posY;
        grille.resetListCoupJouable();
        if((casesVide=grille.listCaseVide())!=null){
            for(int i=0;i<casesVide.size();i++){
                caseTemp=casesVide.get(i);
                posX=caseTemp.getPosX();
                posY=caseTemp.getPosY();
                if(coupValide(1,posX,posY)!=null){
                    caseTemp.setJouableJ1(true);
                    grille.addCoupJouablesJ1(caseTemp);
                }
                else{
                    caseTemp.setJouableJ1(false);
                }
                if(coupValide(2,posX,posY)!=null){
                    caseTemp.setJouableJ2(true);
                    grille.addCoupJouablesJ2(caseTemp);
                }
                else{
                    caseTemp.setJouableJ2(false);
                }
            }
        }
    }
    /*Retourne -1 si la partie n'est pas terminée et le numéro du joueur si un des deux joueur a gagné, retourne 0 en cas d'égalité*/
    public int partieFinie(){
        Log.d("gagnant :j1", Integer.toString(grille.getListCoupJouablesJ1().size()));
        Log.d("gagnant :j2", Integer.toString(grille.getListCoupJouablesJ2().size()));
        if(grille.getListCoupJouablesJ1().isEmpty() && grille.getListCoupJouablesJ2().isEmpty()){
            if(comptePions(1)>comptePions(2)){
                return 1;
            }
            else if(comptePions(1)<comptePions(2)){
                return 2;
            }
            else{
                return 0;
            }
        }
        else{
            return -1;
        }
    }
    public Grille getGrille() {
        return grille;
    }
    public void moteurRandom(){
        IA robot=new IARandom();

        int currentJoueur=1;
        while (partieFinie()==-1){

        }
    }
    public Case getCase(int x,int y){
        return grille.getCase(x,y);
    }

    public int nextJoueur(int j){
        if(j==1){
            return 2;
        }
        else{
            return 1;
        }
    }
    public ArrayList<Case>getListCoupJouable(int joueur){
        if(joueur==1){
            return grille.getListCoupJouablesJ1();
        }
        else if(joueur==2){
            return grille.getListCoupJouablesJ2();
        }
        else{
            return null;
        }
    }
}
