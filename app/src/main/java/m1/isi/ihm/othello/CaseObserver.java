package m1.isi.ihm.othello;

import m1.isi.ihm.othello.jeu.SujetCase;

/**
 * Created by tbriand on 24/02/16.
 */
public interface CaseObserver {
    void actualise(SujetCase caseJeu);
}
