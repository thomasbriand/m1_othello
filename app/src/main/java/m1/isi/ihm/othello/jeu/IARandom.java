package m1.isi.ihm.othello.jeu;

import java.util.List;
import java.util.Random;

/**
 * Created by tbriand on 09/02/16.
 */
public class IARandom implements IA{
    @Override
    public Case coupAJoue(List<Case> listCoupValide,MainJeu etatJeu) {
        Random rand = new Random();
        int randomIndex= rand.nextInt(listCoupValide.size());
        return listCoupValide.get(randomIndex);
    }
}
