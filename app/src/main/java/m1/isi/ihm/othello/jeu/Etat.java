package m1.isi.ihm.othello.jeu;

/**
 * Created by tbriand on 02/02/16.
 */
public interface Etat {
    /*Retourne vrai si le joueur 1 peut poser un pion ici*/
    public boolean isJouableJ1();
    /*Retourne vrai si le joueur 2 peut poser un pion ici*/
    public boolean isJouableJ2();
    /*Inverse la valeur de la case si elle est occupée et retourne le le joueur auquel elle appartient si elle occupée,
    * retourne 0 si elle est vide
    * */
    public int retourne();
    /*Modifie la valeur du joueur auquel la case appartient*/
    public Etat joue(int joueur);
    /*Indique si la case est vide*/
    public boolean estVide();
    /*Retourne la valeur du joueur auquel appartient cette case*/
    public int getJoueur();
    public void setJouableJ1(boolean jouableJ1);
    public void setJouableJ2(boolean jouableJ2);
}
