package m1.isi.ihm.othello.jeu;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tbriand on 02/02/16.
 */
public class Grille {

    private ArrayList<ArrayList<Case>> tabCase;
    private int dimensionX,dimensionY;
    private ArrayList<Case>listCoupJouablesJ1;
    private ArrayList<Case>listCoupJouablesJ2;

    public int getDimensionX() {
        return dimensionX;
    }

    public int getDimensionY() {
        return dimensionY;
    }


    public Grille(Grille toClone){
        dimensionX=toClone.getDimensionX();
        dimensionY=toClone.getDimensionY();
        tabCase=new ArrayList<ArrayList<Case>>();
        ArrayList<Case> temp;
        for(int i=0;i<dimensionX;i++){
            temp=new ArrayList<Case>();
            for(int j=0;j<dimensionY;j++){
                toClone.getCase(i,j).getJoueur();
                temp.add(new Case(toClone.getCase(i, j).getJoueur(), i, j));
            }
            tabCase.add(temp);
        }
    }

    public Grille(List<String> data){
        ArrayList<String>dataInList=new ArrayList<String>(data);
        Log.d("debug","nb ligne data string"+dataInList.size());
        dimensionX=dataInList.size();
        dimensionY=dataInList.size();
        tabCase=new ArrayList<ArrayList<Case>>();
        ArrayList<Case>temp;
        for(int cpt=0;cpt<dimensionX;cpt++){
            temp=new ArrayList<Case>();
            for(int cpt2=0;cpt2<dimensionY;cpt2++){
                temp.add(new Case(Integer.parseInt(dataInList.get(cpt).substring(cpt2,cpt2+1)),cpt,cpt2));
            }
            tabCase.add(temp);
        }


    }
    /*dimx et dimY sont forcément pair*/
    public Grille(int dimX,int dimY){
        dimensionY=dimY;
        dimensionX=dimX;
        listCoupJouablesJ1=new ArrayList<Case>();
        listCoupJouablesJ2=new ArrayList<Case>();
        ArrayList<Case>temp;
        tabCase=new ArrayList<ArrayList<Case>>();
        /*Initialisation de la grille avec que des cases vide*/
        for(int cpt=0;cpt<dimX;cpt++){
            temp=new ArrayList<Case>();
            for(int cpt2=0;cpt2<dimY;cpt2++){
                temp.add(new Case(0,cpt,cpt2));
            }
            tabCase.add(temp);
        }
        /*Ajout des pions de départ*/
        tabCase.get(dimX/2-1).get(dimY/2-1).joue(1);
        tabCase.get(dimX/2).get(dimY/2).joue(1);
        tabCase.get(dimX/2-1).get(dimY/2).joue(2);
        tabCase.get(dimX/2).get(dimY/2-1).joue(2);
        /*initialisation des coups jouables pour les deux joueurs*/
        listCoupJouablesJ1.add(tabCase.get(dimX/2-2).get(dimY/2));
        listCoupJouablesJ1.add(tabCase.get(dimX/2-1).get(dimY/2+1));
        listCoupJouablesJ1.add(tabCase.get(dimX/2).get(dimY/2-2));
        listCoupJouablesJ1.add(tabCase.get(dimX/2+1).get(dimY/2-1));

        listCoupJouablesJ2.add(tabCase.get(dimX/2-1).get(dimY/2-2));
        listCoupJouablesJ2.add(tabCase.get(dimX/2-2).get(dimY/2-1));
        listCoupJouablesJ2.add(tabCase.get(dimX/2+1).get(dimY/2));
        listCoupJouablesJ2.add(tabCase.get(dimX/2).get(dimY/2+1));

    }
    public Case getCase(int x, int y){
        return tabCase.get(x).get(y);
    }
    public ArrayList<Case>getColonne(int x){
        return tabCase.get(x);
    }
    public ArrayList<Case>getLigne(int y){
        ArrayList<Case>res=new ArrayList<Case>();
        for(int i=0;i<dimensionX;i++){
            res.add(tabCase.get(i).get(y));
        }
        return res;
    }
    public ArrayList<Case> getListDansDirection(int posX,int posY,String direction){
        int incrementX=0,incrementY=0;
        int parcoursX,parcoursY;
        ArrayList<Case>res=new ArrayList<Case>();
        switch (direction) {
            case "est":
                incrementX = 1;
                break;
            case "ouest":
                incrementX = -1;
                break;
            case "nord":
                incrementY = -1;
                break;
            case "sud":
                incrementY = 1;
                break;
            case "sud_est":
                incrementX = 1;
                incrementY = 1;
                break;
            case "nord_est":
                incrementX = 1;
                incrementY = -1;
                break;
            case "sud_ouest":
                incrementX = -1;
                incrementY = 1;
                break;
            case "nord_ouest":
                incrementX = -1;
                incrementY = -1;
                break;
        }
        parcoursX=posX+incrementX;
        parcoursY=posY+incrementY;
        while(parcoursX>=0 && parcoursX<dimensionX && parcoursY>=0 && parcoursY<dimensionY){
            res.add(tabCase.get(parcoursX).get(parcoursY));
            parcoursX=parcoursX+incrementX;
            parcoursY=parcoursY+incrementY;
        }
        return res;
    }
    public int comptePions(int joueur){
        int cpt=0;
        for(int i=0;i<tabCase.size();i++){
            for(int j=0;j<tabCase.get(i).size();j++){
                if(tabCase.get(i).get(j).getJoueur()==joueur){
                    cpt++;
                }
            }
        }
        return  cpt;
    }
    public ArrayList<Case> listCaseVide(){
        ArrayList<Case>casesVide=new ArrayList<Case>();
        Case caseTemp;
        for(int i=0;i<tabCase.size();i++){
            for(int j=0;j<tabCase.get(i).size();j++){
                if((caseTemp=tabCase.get(i).get(j)).estVide()){
                    casesVide.add(caseTemp);
                }
            }
        }
        if(!casesVide.isEmpty()){
            return casesVide;
        }
        else{
            return null;
        }
    }
    public void addCoupJouablesJ1(Case coup){
        listCoupJouablesJ1.add(coup);
    }
    public void addCoupJouablesJ2(Case coup){
        listCoupJouablesJ2.add(coup);
    }
    public void resetListCoupJouable(){
        Log.d("debug","Reset list coup jouable");
        listCoupJouablesJ1=new ArrayList<Case>();
        listCoupJouablesJ2=new ArrayList<Case>();

    }
    public ArrayList<Case> getListCoupJouablesJ1() {
        return listCoupJouablesJ1;
    }

    public ArrayList<Case> getListCoupJouablesJ2() {
        return listCoupJouablesJ2;
    }
    public String toString(){
        String res="";
        for(int i=0;i<tabCase.size();i++){
            for (int j =0; j<tabCase.get(i).size();j++){
                res=res+tabCase.get(i).get(j).toString();
            }
            res=res+"\n";
        }

        return res;
    }
}
