package m1.isi.ihm.othello.controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import m1.isi.ihm.othello.CaseLayout;
import m1.isi.ihm.othello.R;
import m1.isi.ihm.othello.activity.Activity_jeu;
import m1.isi.ihm.othello.jeu.Case;
import m1.isi.ihm.othello.jeu.IA;
import m1.isi.ihm.othello.jeu.IAmoyenne;
import m1.isi.ihm.othello.jeu.MainJeu;

/**
 * Created by tbriand on 01/03/16.
 */
public class ControllerJeu {
    private int dimension;
    private int currentJoueur;
    private MainJeu gameEngine;
    private GridLayout grilleLayout;
    private boolean ia;
    private IA joueurOrdinateur;
    private Activity_jeu activity_jeu;


    public int getCurrentJoueur() {
        return currentJoueur;
    }

    public MainJeu getGameEngine() {
        return gameEngine;
    }

    public GridLayout getGrilleLayout() {
        return grilleLayout;
    }

    public ControllerJeu(Activity_jeu activityJeu,Intent intent){
        currentJoueur=1;
        String enemy;
        String imagePion1="";
        String imagePion2="";
        dimension=intent.getIntExtra("xSize", 8);
        enemy=intent.getStringExtra("enemy");
        imagePion1=intent.getStringExtra("colorP1");
        imagePion2=intent.getStringExtra("colorP2");
        if(imagePion1==null){
            imagePion1="";
        }
        if(imagePion2==null){
            imagePion2="";
        }
        if(enemy==null){
            enemy="IA (easy)";
        }
        Log.d("intent",Integer.toString(intent.getIntExtra("enemy",2)));
        this.activity_jeu=activityJeu;
        gameEngine=new MainJeu(dimension,dimension);
        Log.d("enemy",enemy);
        if(!enemy.equals("Human")){
            switch (enemy){
                case "IA (easy)":
                    joueurOrdinateur=new IAmoyenne(1,gameEngine);
                    break;
                case "IA (normal)":
                    joueurOrdinateur=new IAmoyenne(3,gameEngine);
                    break;
                case "IA (hard)":
                    joueurOrdinateur=new IAmoyenne(5,gameEngine);
                    break;
            }
            ia=true;
        }
        else{

            ia=false;
        }
        Log.d("ia init",Boolean.toString(ia));
        grilleLayout = new GridLayout(activityJeu);
        grilleLayout.setColumnCount(dimension);
        grilleLayout.setRowCount(dimension);
        grilleLayout.setUseDefaultMargins(true);
        Log.d("creation","grillelayout construct OK");
        CaseLayout cellLayout=new CaseLayout(activityJeu);
//        Button cell =new Button(this);
        int pos=0;
        for(int i=0;i<dimension;i++){
            for(int j=0;j<dimension;j++){
                Log.d("creation", "I" + i + ", J" + j);
                GridLayout.Spec rowspecs = GridLayout.spec(i);
                GridLayout.Spec colspecs = GridLayout.spec(j);
                GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(rowspecs, colspecs);
                cellLayout=new CaseLayout(activityJeu);
                cellLayout.setPosX(i);
                cellLayout.setPosY(j);
                cellLayout.defPion(imagePion1, 1);
                cellLayout.defPion(imagePion2,2);
                Case caseJeu =gameEngine.getCase(i,j);
                caseJeu.registerObserver(cellLayout);
                cellLayout.actualise(caseJeu);
                Log.d("creation", "actualise OK");
                cellLayout.setLayoutParams(gridLayoutParam);
                cellLayout.setOnClickListener(clickCase);
                Log.d("creation", "setOnclickListner ok");
                cellLayout.setId(pos);
                pos++;
                cellLayout.setPadding(10, 10, 10, 10);
                cellLayout.setBackgroundColor(Color.RED);
                grilleLayout.addView(cellLayout, 60, 60);
            }
            Log.d("ogjzeopgj","pkgzgp");
        }

    }

    public synchronized void clickCaseAction(int x, int y){
        int gagnant;
        int pionJ1;
        int pionJ2;
        Toast toast;
        Case caseJeu=gameEngine.getCase(x,y);
        Log.d("debug", "Current joueur : " + currentJoueur);
        Handler handler = new Handler();
        ProgressDialog pd =new ProgressDialog(this.activity_jeu);
        boolean coupJoue;
        if(gameEngine.getListCoupJouable(currentJoueur).isEmpty()){
            Log.d("jeu","aucun coup jouable pour le joueur"+ currentJoueur);
            currentJoueur=gameEngine.nextJoueur(currentJoueur);
        }
        else{
            enableDisableViewGroup(getGrilleLayout(),false);
            if(coupJoue=gameEngine.coupJoue(currentJoueur,x,y)){
                currentJoueur=gameEngine.nextJoueur(currentJoueur);
            }
            else{
                toast= Toast.makeText(activity_jeu, "Coup non valide!" ,Toast.LENGTH_SHORT);
                toast.show();
            }

            Log.d("debug","Current joueur : "+currentJoueur);
            if(currentJoueur==2 && ia){
                Log.d("ia",Boolean.toString(ia));
                if(gameEngine.getListCoupJouable(currentJoueur).isEmpty()){
                    Log.d("jeu","aucun coup jouable pour le joueur"+ currentJoueur);
                    currentJoueur=gameEngine.nextJoueur(currentJoueur);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            coupIA();
                        }
                    },1000);
                }
            }
            enableDisableViewGroup(getGrilleLayout(),true);
        }
        pionJ1=gameEngine.comptePions(1);
        pionJ2=gameEngine.comptePions(2);
        ((TextView)activity_jeu.findViewById(R.id.jeuScoreLayout)).setText("Score: Joueur 1: "+ pionJ1 +"   Joueur 2: "+pionJ2);
        ((TextView)activity_jeu.findViewById(R.id.jeuJoueurLayout)).setText("Joueur actuel: " + currentJoueur);
        gagnant=gameEngine.partieFinie();
        Log.d("gagnant",Integer.toString(gagnant));
        if (gagnant!=-1){
            Log.d("jeu", "partie terminée");
            activity_jeu.goBack(this.grilleLayout);
            AlertDialog.Builder alert =new AlertDialog.Builder(activity_jeu);
            alert.setTitle("Fin de partie!!");
            if(gagnant==0){
                alert.setMessage("Egalité");
            }
            else{
                alert.setMessage("Le joueur "+gagnant +"gagne!");
            }
            alert.setMessage("Are you sure to delete record");
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //do your work here
                    dialog.dismiss();

                }
            });
            alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });

            alert.show();
        }

    }

    public GridLayout.OnClickListener clickCase=new GridLayout.OnClickListener(){
        @Override
        public void onClick(View v) {
            CaseLayout cellLayout=(CaseLayout)v;
            int x=cellLayout.getPosX();
            int y=cellLayout.getPosY();
            clickCaseAction(x,y);

        }
    };
    public void coupIA(){
        Case caseJeu;
        caseJeu=joueurOrdinateur.coupAJoue(gameEngine.getListCoupJouable(currentJoueur),gameEngine);
        Log.d("Debug","IA joue en "+ caseJeu.getPosX() +" : "+caseJeu.getPosY());
        gameEngine.coupJoue(currentJoueur,caseJeu.getPosX(),caseJeu.getPosY());
        currentJoueur=gameEngine.nextJoueur(currentJoueur);

    }
    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++){
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
        viewGroup.setEnabled(enabled);
    }
    public void saveGame(){
        String backup="";
        backup=backup+getCurrentJoueur()+"\n";
        backup=backup+gameEngine.getGrille().toString();
        Log.d("backup",backup);
        writeBackup(backup);
    }
    public void restoreGame(Activity_jeu activityJeu){
        String backup;
        if(!(backup=readBackup()).equals("")){
            Log.d("restore",backup);
            currentJoueur=Integer.parseInt(backup.split("\n")[0]);
            Log.d("debug","currentJoueur:  "+currentJoueur );
            gameEngine=new MainJeu(backup);
            CaseLayout cellLayout=new CaseLayout(activityJeu);
            int pos=0;
            for (int i = 0; i < grilleLayout.getChildCount(); i++) {
                View child = grilleLayout.getChildAt(i);
                child.setEnabled(false);
                child.setVisibility(View.GONE);
            }
            for(int i=0;i<dimension;i++){
                for(int j=0;j<dimension;j++){
                    Log.d("creation", "I" + i + ", J" + j);
                    GridLayout.Spec rowspecs = GridLayout.spec(i);
                    GridLayout.Spec colspecs = GridLayout.spec(j);
                    GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(rowspecs, colspecs);
                    cellLayout=new CaseLayout(activityJeu);
                    cellLayout.setPosX(i);
                    cellLayout.setPosY(j);

                    Case caseJeu =gameEngine.getCase(i,j);
                    caseJeu.registerObserver(cellLayout);
                    cellLayout.actualise(caseJeu);
                    Log.d("creation", "actualise OK");
                    cellLayout.setLayoutParams(gridLayoutParam);
                    cellLayout.setOnClickListener(clickCase);
                    Log.d("creation", "setOnclickListner ok");
                    cellLayout.setId(pos);
                    pos++;
                    cellLayout.setPadding(10, 10, 10, 10);
                    cellLayout.setBackgroundColor(Color.RED);
                    grilleLayout.addView(cellLayout, 60, 60);
                }
            }
        }





    }


    public void writeBackup(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity_jeu.openFileOutput("backup.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private String readBackup() {

        String ret = "";

        try {
            InputStream inputStream = activity_jeu.openFileInput("backup.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                    stringBuilder.append("\n");
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        activity_jeu.deleteFile("backup.txt");

        return ret;
    }

}
