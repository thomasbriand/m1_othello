package m1.isi.ihm.othello.jeu;

import java.util.List;

/**
 * Created by tbriand on 09/02/16.
 */
public interface IA {
    public Case coupAJoue(List<Case> listCoupValide,MainJeu etatJeu);
}
