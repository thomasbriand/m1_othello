package m1.isi.ihm.othello.jeu;

/**
 * Created by tbriand on 02/02/16.
 */
public class CaseVide implements Etat {
    private boolean jouableJ1,jouableJ2;

    public void setJouableJ1(boolean jouableJ1) {
        this.jouableJ1 = jouableJ1;
    }

    public void setJouableJ2(boolean jouableJ2) {
        this.jouableJ2 = jouableJ2;
    }

    @Override
    public boolean isJouableJ1() {
        return jouableJ1;
    }

    @Override
    public boolean isJouableJ2() {
        return jouableJ2;
    }

    @Override
    public int retourne() {
        return 0;
    }

    @Override
    public Etat joue(int joueur) {
        return new CaseOccupee(joueur);
    }

    @Override
    public boolean estVide() {
        return true;
    }

    @Override
    public int getJoueur() {
        return 0;
    }
}
