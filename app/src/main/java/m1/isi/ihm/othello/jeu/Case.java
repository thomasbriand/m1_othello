package m1.isi.ihm.othello.jeu;

import android.util.Log;

import java.util.ArrayList;

import m1.isi.ihm.othello.CaseObserver;

/**
 * Created by tbriand on 02/02/16.
 */
public class Case implements SujetCase{
    private Etat etat;
    private int posX,posY;
    private ArrayList<CaseObserver> listObs;
    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public Case(int joueur,int posX,int posY){
        listObs=new ArrayList<CaseObserver>();
        this.posX=posX;
        this.posY=posY;
        if(joueur==0){
            etat=new CaseVide();
        }
        else{
            etat=new CaseOccupee(joueur);
        }
    }
    public void joue(int joueur){
        etat=etat.joue(joueur);
        notifyObs();
    }
    public boolean isJouableJ1(){
        return etat.isJouableJ1();
    }
    public boolean isJouableJ2(){
        return etat.isJouableJ2();
    }
    public boolean estVide(){
        return etat.estVide();
    }
    public int getJoueur(){
        return etat.getJoueur();
    }
    public int retourne(){
        if((posY==0 && posX==0)||(posY==8 && posX==0)||(posY==8 && posX==8)||(posY==0 && posX==8)){
            Log.d("erreur", "retournement coin");
        }
        int res=etat.retourne();
        notifyObs();
        return res;
    }

    public void setJouableJ1(boolean jouableJ1) {
        etat.setJouableJ1(jouableJ1);
    }
    public void setJouableJ2(boolean jouableJ2) {
        etat.setJouableJ2(jouableJ2);
    }

    @Override
    public void notifyObs() {
        for(CaseObserver obs:listObs){
            obs.actualise(this);
        }
    }

    @Override
    public void registerObserver(CaseObserver obs) {
        listObs.add(obs);
    }

    @Override
    public void removeObserver(CaseObserver obs) {
        if (listObs.contains(obs)){
            listObs.remove(obs);
        }

    }
    public void setEtat(Etat e){
        this.etat=e;
    }
    public Etat getEtat(){
        return etat;
    }
    public String toString(){
        return (new Integer(etat.getJoueur())).toString();
    }
}
