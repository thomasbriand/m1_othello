package m1.isi.ihm.othello.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import m1.isi.ihm.othello.R;

public class PlayMenuActivity extends Activity implements AdapterView.OnItemSelectedListener {

    //Spinner profilep1Spin;
    Spinner profilep2Spin;
    Spinner colorp1Spin;
    Spinner colorp2Spin;
    Spinner themeSpin;
    Spinner sizeSpin;

    //Global
    String colp1;
    String colp2;
    String theme;
    int xSize;
    int ySize;
    //String profilep1;
    String profilep2;

    String[] aradC; //Tableau des couleurs
    String[] aradT; //Tableau des thèmes
    String[] aradP1; //Tableau des profiles utilisateurs
    //List<String> aradP2;
    String[] aradSize; //Tableau des tailles de grille

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_menu);

    //Remplis le spinner pour le profile p2
        /*profilep1Spin = (Spinner) findViewById(R.id.playMenuSpinProfileP1);
        aradP1 = new String[1];
        aradP1[0] = "toto";*/
    //Remplis le spinner pour le profile p2
        aradP1 = getResources().getStringArray(R.array.playMenuP2IAchoice);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, aradP1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        profilep2Spin = (Spinner) findViewById(R.id.playMenuSpinProfileP2);
        profilep2Spin.setAdapter(adapter);
        profilep2Spin.setOnItemSelectedListener(this);
    //Remplis le spinner color 1
        aradC = getResources().getStringArray(R.array.playMenuColorchoice);
        colorp1Spin = (Spinner) findViewById(R.id.playMenuSpinP1Col);
        ArrayAdapter<String> adapterC1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, aradC);
        adapterC1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorp1Spin.setAdapter(adapterC1);
        colorp1Spin.setOnItemSelectedListener(this);
    //Remplis le spinner color 2
        colorp2Spin = (Spinner) findViewById(R.id.playMenuSpinP2Col);
        ArrayAdapter<String> adapterC2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, aradC);
        adapterC2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorp2Spin.setAdapter(adapterC2);
        colorp2Spin.setOnItemSelectedListener(this);
    //Remplis le spinner theme
        themeSpin = (Spinner) findViewById(R.id.playMenuSpinTheme);
        aradT = getResources().getStringArray(R.array.playMenuThemechoice);
        ArrayAdapter<String> adapterT = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, aradT);
        adapterT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        themeSpin.setAdapter(adapterT);
    //Remplis le spinner Size
        sizeSpin = (Spinner) findViewById(R.id.playMenuSpinSize);
        aradSize = getResources().getStringArray(R.array.playMenuSizechoice);
        ArrayAdapter<String> adapterSize = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, aradSize);
        adapterSize.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeSpin.setAdapter(adapterSize);
        sizeSpin.setOnItemSelectedListener(this);
    }

    public void startBtnClicked(View v){
        Intent intent = new Intent(PlayMenuActivity.this, Activity_jeu.class);
        intent.putExtra("colorP1", colp1);
        intent.putExtra("colorP2", colp2);
        intent.putExtra("theme", theme);
        intent.putExtra("enemy", profilep2);
        intent.putExtra("xSize", xSize);
        intent.putExtra("ySize", ySize);
        intent.putExtra("newGame", true);
        startActivity(intent);
    }
    public void cancelBtnClicked(View v){
        Intent intent = new Intent(PlayMenuActivity.this, MainActivity.class);
        startActivity(intent);
    }
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        Spinner spinner = (Spinner) parent;
        Log.d("Event", "onItemSelected for: "+parent.getId());
        switch (parent.getId())
        {
            case R.id.playMenuSpinP1Col:

                colp1 = aradC[pos];
                break;

            case R.id.playMenuSpinP2Col:
                colp2 = aradC[pos];
                break;
            case R.id.playMenuSpinTheme:
                theme = aradT[pos];

                break;
            case R.id.playMenuSpinProfileP2:
                profilep2 = aradP1[pos];
                break;
            case R.id.playMenuSpinSize:
                String[] size = aradSize[pos].split(" x ");
                xSize = Integer.parseInt(size[0]);
                ySize = Integer.parseInt(size[1]);
                break;
        }
    }
    public void onNothingSelected(AdapterView<?> parentView){
        //
    }
    public void goBack(View view){finish();}
}
