package m1.isi.ihm.othello.player;

/**
 * Created by Vincent on 11/02/2016.
 * Updated by Vincent on 08/03/2016
 */
public class Profile {
    //Attributs
    private int id;
    private String name;
    private int xp;
    //private truc avatar;

    public Profile(){
        this.xp = 0;
        this.name = "";
    }
    //Constructeur
    public Profile(String name, int xp){
        this.name=name;
        this.xp = xp;
    }
    //Accesseur
    public String getName(){
        return this.name;
    }
    public Profile setName(String name){
        this.name = name;
        return this;
    }
    public int getXP(){
        return this.xp;
    }
    public Profile setXP(int xp){
        if(xp > 0){
            this.xp = xp;
        }
        return this;
    }
    public int getId() {
        return id;
    }

    public Profile setId(int id) {
        this.id = id;
        return this;
    }
}
