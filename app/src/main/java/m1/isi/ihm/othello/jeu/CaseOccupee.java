package m1.isi.ihm.othello.jeu;

/**
 * Created by tbriand on 02/02/16.
 */
public class CaseOccupee implements Etat{
    private int joueur;

    @Override
    public int getJoueur() {
        return joueur;
    }

    public CaseOccupee(int joueur){
        this.joueur=joueur;
    }
    @Override
    public boolean isJouableJ1() {
        return false;
    }
    @Override
    public boolean isJouableJ2() {
        return false;
    }
    @Override
    public void setJouableJ1(boolean jouableJ1) {

    }

    @Override
    public void setJouableJ2(boolean jouableJ2) {

    }

    @Override
    public int retourne() {
        if(joueur == 1){
            joueur = 2;
        }
        else{
            joueur = 1;
        }
        return joueur;
    }

    @Override
    public Etat joue(int joueur) {
        return this;
    }

    @Override
    public boolean estVide() {
        return false;
    }
}
