package m1.isi.ihm.othello.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import m1.isi.ihm.othello.CaseLayout;
import m1.isi.ihm.othello.R;
import m1.isi.ihm.othello.controller.ControllerJeu;
import m1.isi.ihm.othello.jeu.Case;
import m1.isi.ihm.othello.jeu.Grille;
import m1.isi.ihm.othello.jeu.IA;
import m1.isi.ihm.othello.jeu.IAmoyenne;
import m1.isi.ihm.othello.jeu.MainJeu;

public class Activity_jeu extends Activity {

    private int dimension;
    private ControllerJeu controllerJeu;
    private Boolean newgame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("test", "oncreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_jeu);
        Intent intent= getIntent();

        newgame=intent.getBooleanExtra("newGame", false);
        intent.putExtra("newGame", false);



        controllerJeu=new ControllerJeu(this,intent);
        ((RelativeLayout)findViewById(R.id.jeuLayout)).addView(controllerJeu.getGrilleLayout());
    }


    public void onDestroy(){
        super.onDestroy();
    }


    @Override
    public void onStart(){
        Log.d("test", "onstart");


        super.onStart();
    }

    @Override
    public void onResume(){
        Log.d("test", "onresume");


        if(controllerJeu!=null && !newgame){
            Log.d("debug", "controlleur not null");
            controllerJeu.restoreGame(this);

        }
        newgame=false;
        Log.d("newgame", newgame.toString());


        super.onResume();
    }

    @Override
    public void onPause(){
        /*Ecriture du fichier contenant les informations de jeu*/
        Log.d("test","onpause");
        super.onPause();
    }
    @Override
    public void onStop(){

        Log.d("test","onstop");
        controllerJeu.saveGame();
        super.onStop();
    }
    public void goBack(View view){finish();}

}
